#include <Arduino.h>
#include <DHT.h>
#include <LiquidCrystal.h>
#include <ESP8266WiFi.h>
#include "ThingSpeak.h"
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

WiFiServer server(80);

/* NETWORK SETUP */
String apiKey = "BR3SSYSRLEOBD533";
String apiKeyBuzzer = "E5UKEEEYSHFY9TJL";

const char *ssid = "MERKEZ";
const char *pass = "3005200730052007";
const char* serverThingSpeak = "api.thingspeak.com";

WiFiClient client;

unsigned long counterChannelNumber = 1041417;
const char * myCounterReadAPIKey = "Q8WW7IHMD9SI3YH3";
const int FieldNumber = 1;
int statusCode = 0;
/* ************ */

/* LCD SETUP */
LiquidCrystal lcd(13,12,4,0,2,14);
/* ********* */

/* DHT SETUP */
#define DHTTYPE DHT22
#define DHTPIN D1
#define buzzPin 15
#define buttonPin 16

float hum=0;
float temp=0;
float tempMax=0;
float humMax=0;
float tempMin=100;
float humMin=100;

float chk;
float h, t;

DHT dht(DHTPIN, DHTTYPE); 
/* ********* */

/* DELAY SETUP */
unsigned long curTime;
boolean flag = false;
/* *********** */

void setup() {
  pinMode(buzzPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  Serial.begin(115200);
  dht.begin();
  lcd.begin(16, 2);
  ThingSpeak.begin(client);
  WiFiManager wifiManager;

  lcd.clear();
  lcd.print("BASLATILIYOR...");
  lcd.setCursor(0,1);
  lcd.print("CITY HOSPITAL");

  delay(100);
  
  if(digitalRead(buttonPin)==HIGH)
  {
    lcd.clear();
    lcd.print("SIFIRLANIYOR.");
    wifiManager.resetSettings();
  }

  delay(2000);

  lcd.clear();
  lcd.print("WiFi AGINA");
  lcd.setCursor(0,1);
  lcd.print("BAGLANIN!");

  //wifiManager.resetSettings();
  wifiManager.autoConnect("Kat 2 Server Isı Nem");
  Serial.println("Connected.");
  lcd.clear();
  lcd.print("BAGLANDI!");
  server.begin();
  
  delay(2000);
}

void loop() {
  flag = false;
  client = server.available();
  //----------------- Network -----------------//
  /*
  if (WiFi.status() != WL_CONNECTED)
  {
    Serial.println();
    Serial.print("Connecting to ");
    Serial.print(ssid);
    Serial.println(" ....");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("WIFI AGINA");
    lcd.setCursor(0,1);
    lcd.print("BAGLANILIYOR...");
    while (WiFi.status() != WL_CONNECTED)
    {
      WiFi.begin(ssid, pass);
      Serial.print(".");
      delay(5000);
    }
    Serial.println("Connected to Wi-Fi Succesfully.");
  }
  */
  //--------- End of Network connection--------//
  
  chk = dht.read(DHTPIN);
  h = dht.readHumidity();
  t = dht.readTemperature();  
  if (isnan(h) || isnan(t))
  {
    while(isnan(h) || isnan(t))
    {
      delay(2000);
      Serial.println("Failed to read from DHT sensor!");
      chk = dht.read(DHTPIN);
      h = dht.readHumidity();
      t = dht.readTemperature(); 
    }
  }
  hum = h;
  temp = t;
    
  if(hum>humMax) humMax=hum;
  if(temp>tempMax) tempMax=temp;
  if(hum<humMin) humMin=hum;
  if(temp<tempMin) tempMin=temp;
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("ISI:");
  lcd.print(temp);
  lcd.setCursor(8,0);
  lcd.print("C  ");
  lcd.print((int)tempMax);
  lcd.print("-");
  lcd.print((int)tempMin);
  lcd.print("C");
  lcd.setCursor(0,1); 
  lcd.print("NEM:");
  lcd.print(hum);  
  lcd.setCursor(8, 1);
  lcd.print("%  ");
  lcd.print((int)humMax);
  lcd.print("-");
  lcd.print((int)humMin);
  lcd.print("%");

  if (isnan(hum) || isnan(temp))
  {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  if (client.connect(serverThingSpeak, 80))
  {
    String postStr = apiKey;
    postStr +="&field1=";
    postStr += String(temp);
    postStr +="&field2=";
    postStr += String(hum);
    postStr += "\r\n\r\n";

    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);

    Serial.print("Sıcaklık: ");
    Serial.print(temp);
    Serial.print(" °C, Nem: ");
    Serial.print(hum);
    Serial.println("%. Thingspeak'e gönderildi.");  
    delay(2000);
  }
  client.stop();

  Serial.println("Waiting...");
  
  curTime = millis();
  while(millis()-curTime <= 1800000)
  {  
    //----------------- Network -----------------//
    if (WiFi.status() != WL_CONNECTED)
    {
      Serial.print("Connecting to ");
      Serial.print(ssid);
      Serial.println(" ....");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("WIFI AGINA");
      lcd.setCursor(0,1);
      lcd.print("BAGLANILIYOR...");
      while (WiFi.status() != WL_CONNECTED)
      {
        WiFi.begin(ssid, pass);
        Serial.print(".");
        delay(5000);
      }
      Serial.println("Connected to Wi-Fi Succesfully.");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("ISI:");
      lcd.print(temp);
      lcd.print("C  ");
      lcd.print(tempMax);
      lcd.print("-");
      lcd.print(tempMin);
      lcd.print("C");
      lcd.setCursor(0,1); 
      lcd.print("NEM:");
      lcd.print(hum);
      lcd.print("%  ");
      lcd.print(humMax);
      lcd.print("-");
      lcd.print(humMin);
      lcd.print("%");
    }
    //--------- End of Network connection--------//
    //---------------- CRITICAL ----------------//
    delay(2000);
    chk = dht.read(DHTPIN);
    h = dht.readHumidity();
    t = dht.readTemperature();  
      
    if (isnan(h) || isnan(t))
    {
      while(isnan(h) || isnan(t))
      {
        delay(2000);
        Serial.println("Failed to read from DHT sensor!");
        chk = dht.read(DHTPIN);
        h = dht.readHumidity();
        t = dht.readTemperature();
      }
    }
    hum = h;
    temp = t;
    Serial.print("Sıcaklık: ");
    Serial.print(temp);
    Serial.print(" °C, Nem: ");
    Serial.print(hum);
    Serial.println("%");

    if(temp >= 45 && flag == false)
    {
      tone(buzzPin, 4000);
      if (client.connect(serverThingSpeak, 80))
      {
        String postStr = apiKey;
        postStr +="&field1=";
        postStr += String(temp);
        postStr += "\r\n\r\n";
    
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(postStr.length());
        client.print("\n\n");
        client.print(postStr);
    
        Serial.print("KRITIK SICAKLIK UYARISI: ");
        Serial.println(temp);
        delay(2000);
      }
      client.stop();
      flag = true;
      if(temp>tempMax) tempMax = temp;
      lcd.setCursor(0,0);
      lcd.print("ISI:");
      lcd.print(temp);
      lcd.print("C  ");
      lcd.print(tempMax);
      lcd.print("-");
      lcd.print(tempMin);
      lcd.setCursor(0,1); 
      lcd.print("NEM:");
      lcd.print(hum);
      lcd.print("%  ");
      lcd.print(humMax);
      lcd.print("-");
      lcd.print(humMin);
      lcd.print("%");
    }
    
    //---------------- END CRITICAL ------------//
    
    //---------------- Buzzer ON-OFF ----------------//
    long buzzerState = ThingSpeak.readLongField(counterChannelNumber, FieldNumber, myCounterReadAPIKey);
    statusCode = ThingSpeak.getLastReadStatus();
    if (statusCode == 200 && buzzerState == 1)
    {
      Serial.print("Buzzer State: ");
      Serial.println(buzzerState);

      delay(20000);
      tone(buzzPin,4000);      
      if (client.connect(serverThingSpeak, 80))
      {
        String postStr = apiKeyBuzzer;
        postStr +="&field1=";
        postStr += String(0);
        postStr += "\r\n\r\n";
      
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: "+apiKeyBuzzer+"\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(postStr.length());
        client.print("\n\n");
        client.print(postStr);
        Serial.println("flag");
        delay(2000);
      }
      client.stop();
    }
    //-------------- End of Buzzer ON-OFF -------------//
  
    if(digitalRead(buttonPin)==HIGH)
    {
      Serial.println("Butona basıldı");
      noTone(buzzPin);
    }
    
    if(curTime >= 2592000000)
    {
      ESP.restart();
    }
    
    yield();
  } 
}